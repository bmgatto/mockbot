#!/usr/bin/python3

import random
import os 
import re

def stringReverserer(lthing):
    reversedString = []
    for i in range(0,len(lthing)):
        r = random.randint(1,2)
        if r % 2 == 0:
            reversedString.append(lthing[i].upper())
        else:
            reversedString.append(lthing[i].lower())
    lfinal = ''.join(reversedString)
    return lfinal

print("Hello! This is a state of the art chatbot")
print("that is able to have complete conversations")
print("at the level of the average college student.")
print("Go ahead, introduce yourself!")

while True:
    thing = input("> ")

    while True:
        final = stringReverserer(thing)
        if re.search(r'[A-Z]{3,}|[a-z]{3,}',final):
            final = stringReverserer(thing)
        else:
            break

    os.system("echo " + final + " | cowsay")
